# Kirby Docker

This is allows a kirby instance to run with a docker volume on linux.
Normally there would be a clash with the user ID and group ID when 
mounting on a docker volume, because the host uid and gid of the files
are passed through natively to the container. When the uid and gid 
are not correct the PHP container the PHP process cannot write assets to the web root

This is an addition on the ` php:7.3.5-fpm-alpine3.9` container.

There is also an nginx container with plain HTTP 

## Requirements
- [ahoy](https://github.com/ahoy-cli/ahoy)
    - If you don't want to install `ahoy` all the normal docker commands are in `.ahoy.yml`
- [docker](https://www.docker.com/)
 
## Usage
### Initial run:
1. `git clone --recursive https://gitlab.com/max-ar/kirby-docker.git directory_name`
1. `cd directory_name`
1. `ahoy up`
1. In your browser go to [http://127.0.0.1:8080](http://127.0.0.1:8080)

If you have old versions of the container in your docker registry 
you may need to run the following command;
- `ahoy rebuild`

### Using your own kriby project
To use your own kirby project;
```bash
git submodule deinit -f kirby/
rm -rf .git/modules/kirby/
git rm -f kirby/
mkdir kirby/
``` 

Note that you must create your new project in the `kriby/` directory.

## TODO:
1. Add letsencrypt to the nginx container. 
1. Allow access to nginx from all domains, not just `127.0.0.1` and `localhost`

## Handy Commands
Rebuild the containers;
- `ahoy rebuild`

Get a bash session into the PHP container;
- `ahoy cli`

View the logs;
- `ahoy logs`
- `ahoy logs container_name`

## Versions
Tested on;
- `docker-compose version 1.23.2`
- `Docker version 18.09.6`