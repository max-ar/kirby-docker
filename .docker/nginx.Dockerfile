FROM nginx:1.17-alpine

ARG USER
ARG GROUP

COPY /.docker/nginx/nginx.conf /etc/nginx/conf.d/nginx.conf

RUN sed -i "s/user  nginx;/user $USER;/" /etc/nginx/nginx.conf

RUN addgroup --gid "$GROUP" "$USER" \
    && adduser \
    --disabled-password \
    --gecos "" \
    --home "$(pwd)" \
    --ingroup "$USER" \
    --no-create-home \
    --uid "$USER" \
    "$USER"

RUN apk update
RUN apk upgrade
RUN apk add bash