FROM php:7.3.5-fpm-alpine3.9

ARG USER
ARG GROUP

COPY / /app
COPY /.docker/php/www.conf /usr/local/etc/php-fpm.d/www.conf

RUN sed -i "s/PHP_FPM_USER/$USER/" /usr/local/etc/php-fpm.d/www.conf
RUN sed -i "s/PHP_FPM_GROUP/$GROUP/" /usr/local/etc/php-fpm.d/www.conf

RUN apk update
RUN apk upgrade
RUN apk add bash